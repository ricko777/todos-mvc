import org.junit.*;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.html5.LocalStorage;

public class Main {

    WebDriver driver;
    JavascriptExecutor js;
    @BeforeClass
    public static void mainPrecondition(){
        System.setProperty("webdriver.gecko.driver", "d:\\geckodriver\\geckodriver.exe");
    }
    @Before
    public void preCondition(){
        driver = new FirefoxDriver();
    }
    @Test
    public void todomvc(){
    driver.get("http://todomvc.com/examples/backbone/");
    driver.findElement(By.className("new-todo")).sendKeys("1", Keys.ENTER);
    driver.findElement(By.className("new-todo")).sendKeys("2", Keys.ENTER);
    driver.findElement(By.className("new-todo")).sendKeys("3", Keys.ENTER);
    driver.findElement(By.className("new-todo")).sendKeys("4", Keys.ENTER);
    driver.findElement(By.className("new-todo")).sendKeys("5", Keys.ENTER);

        String counter = driver.findElement(By.className("todo-count")).findElement(By.tagName("strong")).getText();
        System.out.println(counter);
        Assert.assertEquals("Unexpected string value", "5", counter);
    }
    @After
    public void postCondition(){


    }
}
